# Iota

Play with Iota here: [https://owen.cafe/iota/](https://owen.cafe/iota/)

## Technologies

* Frontend: [Elm](http://elm-lang.org/)
* Backend: [Node.js](https://nodejs.org/en/)
* Networking: [WebSockets](https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API)

Elm is a lovely language, and made this project a pleasure to create <3
